import React, {Component} from 'react'
import './button_style.css'

class Login extends Component{


    render(){
        return(
            <div>
                <button 
                    className='__Login'
                
                >Login</button>
                &nbsp;&nbsp;
                <input 
                    type='submit'
                    className='__Submit'
                /><br/>
                <input 
                    type='reset'
                    className='__Reset'
                />
                &nbsp;&nbsp;
                <button
                    className='__Cancel'
                >Cancel</button>
            </div>
        )
    }
}

export default Login