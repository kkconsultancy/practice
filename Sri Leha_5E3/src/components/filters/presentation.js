import React, { Component } from 'react'

class Presentation extends Component{

    render(){ 
        //console.log("present--"+this.props.data)
       
        return( 
            
            <div>
                
                <button name="title-aeiou" onClick={this.props.submit}>Titles with aeiou</button>&nbsp;&nbsp;
                <button name="Even_id" onClick={this.props.submit}>Even AlbumID</button>&nbsp;&nbsp;
                <button name="Title_Caps" onClick={this.props.submit}>CAPS Title</button>&nbsp;&nbsp;<br/><br/>
                Search:<input type="text" name="search" value={this.props.value} onChange={this.props.submit}/><br/><br/><br/>
                {this.props.data2}
            </div>
          
        )
    }
}

export default Presentation;
