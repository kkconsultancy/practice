import React,{Component} from 'react'
import axios from 'axios'
import Presentation from './presentation'

class Container extends Component{
     
    state={}
    componentDidMount =()=>{

        axios.get("https://jsonplaceholder.typicode.com/photos",{})
        .then(response=>{
            const data =response.data
            this.op= data.map(output=>{
                return this.display(output)
            })
            this.setState({
                data: data,
                abc: this.op
            })
            console.log(data)

        }).catch(err=>{
            console.log(err)
        }) 
    }

    submit=(e)=>{
        console.log("HEllo..")
        const name =e.target.name
        const value =e.target.value
        this.setState({
           [name]:value
        })
        this.apply(e)
    }
    
    apply =(e)=>{
        var name= e.target.name;
        //var count=0;
        
        if(name==="title-aeiou"){
           
            let value= e.target.value;
            let text="Records with title-aeiou"
            console.log("value--"+value);
            const data2= this.state.data.slice(0,20).filter(res=>res.title.match(/^[aeiou]/)).map(records=>{
               
                return (this.display(records,text))
            })
            this.setState({
                data2: data2
            })
        }
        else if(name==="Even_id"){
            let text="Albums with even ID"
            console.log(text)
            const data2= this.state.data.slice(0,20).filter(res=>res.id%2===0).map(records=>{
                return (this.display(records,text))

            })
            this.setState({
                data2: data2
            })
        }
        else if(name==="Title_Caps"){
            let text="Albums with Title Capitalized"
            console.log(text)
            const data2= this.state.data.slice(0,20).map(records=>{
                return (this.display(records,text))

            })
            this.setState({
                data2: data2
            })
        }
        else if(name==="search"){
            let value=e.target.value;
            let text="Search results "
            console.log(text)
            const data2= this.state.data.slice(0,20).filter(res=>res.title.match(value)).map(records=>{
                return (this.display(records,text))

            })
            this.setState({
                data2: data2
            })
        }else{
            this.setState({
                data2: ""
            })
        }
            
        
    }

    display =(r,text)=>{
        console.log("Display::"+text)  
        if(text==="Albums with Title Capitalized"){
            return(
                <div>
                   <div><b><u>{text}</u></b><br/><br/></div>
                   <div> 
                        {/*<b>S.NO:</b>{count=count+1}<br/>*/}
                        <b>AlbumID:</b>{r.id}<br/>
                        <b>Title:</b>{r.title.toUpperCase()}<br/>
                        <b>Photo:</b><img src={r.url} height="100px" width="100px"/>
                    </div><br/>
                    <hr/>
                </div>
            )
        }
        return(
            <div>
               <div><b><u>{text}</u></b><br/><br/></div>
               <div> 
                    {/*<b>S.NO:</b>{count=count+1}<br/>*/}
                    <b>AlbumID:</b>{r.id}<br/>                    
                    <b>Title:</b>{r.title}<br/>
                    <b>Photo:</b><img src={r.url} height="100px" width="100px"/>
                </div><br/>
                <hr/>
            </div>
        )
    }
    render(){
        return(
           
            <Presentation
                {...this.props}
                {...this.state}
                submit={this.submit}
            />
            
            
        )
    }
}

export default Container;