import React from 'react'
class xyz extends React.Component{
    
    state={}
    componentDidMount=()=>{
        const { Parser } = require('json2csv');
        const fields = ['id', 'name', 'class','email','grade'];
        const jsonFile = [ 

            {
                "id": 1,
                "name":"abc",
                "class": "IV",
                "email": "abc@edu.com",
                "grade": "A"
            },
            {
                "id": 2,
                "name":"xyz",
                "class": "IV",
                "email": "xyz@edu.com",
                "grade": "C"
            },
            {
                "id": 3,
                "name":"pqr",
                "class": "IV",
                "email": "pqr@edu.com",
                "grade": "B"
            }
        ]
 
        var obj=JSON.stringify(jsonFile) //JSON to string
        console.log("type-"+typeof(obj)+"-> "+obj) 

        var objarr= JSON.parse(obj) //JSON str to Arr obj
        console.log("type-"+typeof(objarr)) 
        console.log(objarr)

        const json2csvParser = new Parser({ fields }); //JSON to CSV
        const csv = json2csvParser.parse(jsonFile);
 
        console.log(typeof(csv)+"->")
        
        let csvContent = "data:text/csv;charset=utf-8," + csv;
        var encodedUri = encodeURI(csvContent);
        var link = document.getElementById("csvdownload");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "my_data.csv");
        link.textContent = "DOWNLOAD HERE"
        // document.body.appendChild(link); // Required for FF
        console.log(link)
        
    }

render() {
    return(
        <div>
            <a id="csvdownload"></a>
        </div>
    )
}
}
export default xyz;