import React,{Component} from 'react'
import './text_input_style.css'

class Textarea extends Component{
    constructor(props){
        super(props)
        this.state ={
            comment:''
        }
        this.handleInput=this.handleInput.bind(this)
    }
    handleInput = (e)=>{
        this.setState({
            comment: e.target.value
        })
    }
    render(){
        return(
            <div> 
                <textarea  
                    name='comment'
                    rows='7' cols='30' 
                    value={this.state.comment}
                    placeholder='your comment...'
                    onChange = {this.handleInput}
                    className='__textarea_input'
                />

            </div>
        )
    }
}

export default Textarea