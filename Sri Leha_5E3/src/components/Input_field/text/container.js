import React,{Component} from 'react'
import './text_input_style.css'

class Text extends Component{
    constructor(props){
        super(props)
        this.state ={
            uname:''
        }
        this.handleInput=this.handleInput.bind(this)
    }
    handleInput = (e)=>{
        this.setState({
            uname: e.target.value
        })
    }
    render(){
        return(
            <div>
                <input 
                    type='text' 
                    name='uname' 
                    value={this.state.uname}
                    placeholder='uname'
                    onChange = {this.handleInput}
                    className='__text_input'
                />

            </div>
        )
    }
}

export default Text