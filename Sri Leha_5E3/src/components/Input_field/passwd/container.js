import React,{Component} from 'react'
import './passwd_input_style.css'

class Passwd extends Component{
    constructor(props){
        super(props)
        this.state ={
            pwd:''
        }
        this.handleInput=this.handleInput.bind(this)
    }
    handleInput = (e)=>{
        this.setState({
            pwd: e.target.value
        })
    }
    render(){
        return(
            <div>
                <input 
                    type='password'  
                    name='pwd' 
                    value={this.state.pwd}
                    placeholder='********'
                    onChange = {this.handleInput}
                    className='__pwd_input'
                />

            </div>
        )
    }
}

export default Passwd