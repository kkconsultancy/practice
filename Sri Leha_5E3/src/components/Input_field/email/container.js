import React,{Component} from 'react'
import './email_input_style.css'

class EmailInput extends Component{
    constructor(props){
        super(props)
        this.state ={
            email:''
        }
        this.handleInput=this.handleInput.bind(this)
    }
    handleInput = (e)=>{
        this.setState({
            email: e.target.value
        })
        
    }
    render(){
        return(
            <div>
                <input 
                    type={this.props.type} 
                    name={this.prop.name} 
                    value={this.state.email}
                    placeholder={this.state.placeholder}
                    onChange = {this.handleInput}
                    className='__email_input'
                />

            </div>
        )
    }
}

export default EmailInput