import React from 'react';
import axios from 'axios'

class A extends React.Component{
    state = {}
    componentDidMount = () => {
        axios.get("https://jsonplaceholder.typicode.com/posts",{})
        .then((response) => {
            const data = response.data
            console.log(data)
            this.output = data.filter(p => p.title.match(/^[aeio]/)).map(p => {
                return this.display(p)
            })
            this.setState({
                abc : this.output
            })
            // console.log(output)
        }).catch(err => {
            console.log(err)
        })
    }

    display = (post) =>{
        return(
            <div>
                <div>Userid : {post.userId}</div>
                <div>Title : {post.title}</div>
            </div>
        )
    }
    output = ""
    render(){
        return(
            <div>{this.state.abc}</div>
        )
    }
}
export default A;