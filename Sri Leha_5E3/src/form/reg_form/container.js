import React,{Component} from 'react'
import Presentation from './presentation'

class Container extends Component{

    state={}
    handleEvent = (e)=>{
        const name=e.target.name;
        const value=e.target.value;
        this.setState(
            {
                [name]:value
            }
        )
        console.log(this.state);
    }
    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
            />
        )
    }
}

export default Container