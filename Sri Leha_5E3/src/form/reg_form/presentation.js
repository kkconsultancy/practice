import React,{component} from 'react'

class Presentation extends Component{

    render(){
        return(
            <div>
            <h1>Registration form</h1>
            <form>
                <label>Hallticket Number&nbsp;</label>
                <input type="text" name="Hno" required onChange={this.props.props.handleEvent}/><br/>
                
                <label>FirstName&nbsp;</label><input type="text" name="fname" required onChange={this.props.props.handleEvent}/><br/>
                LastName&nbsp;<input type="text" name="lname" required onChange={this.props.handleEvent}/><br/>
                Gender&nbsp;<input type="radio" name="gen" required onChange={this.props.handleEvent}/>Male&nbsp;
                            <input type="radio" name="gen" required onChange={this.props.handleEvent}/>Female<br/>
                Course&nbsp;<input type="radio" name="course" required onChange={this.props.handleEvent}/>B.Tech&nbsp;
                            <input type="radio" name="course" required onChange={this.props.handleEvent}/>Diploma<br/>
                <label>Department</label>&nbsp;
                <select name="dept"required onChange={this.props.handleEvent}>
                    <option value="---">---</option>
                    <option value="cse">CSE</option>
                    <option value="ece">ECE</option>
                    <option value="eee">EEE</option>
                    <option value="me">ME</option>
                    <option value="ce">CE</option>
                </select><br/>
                <label>Year</label>&nbsp;
                <select  name="yr"required onChange={this.props.handleEvent}>
                    <option value="---">---</option>
                    <option value="1">I</option>
                    <option value="2">II</option>
                    <option value="3">III</option>
                    <option value="4">IV</option>
                </select><br/>
                <label>College</label>&nbsp;
                <select name="clg"required onChange={this.props.handleEvent}>
                    <option value="---">---</option>
                    <option value="site">SITE</option>
                    <option value="vasavi">VASAVI</option>
                    <option value="wise">WISE</option>
                    <option value="visit">VISIT</option>
                </select><br/>
                <label>E-Mail</label>&nbsp;<input type="email" name="mail" required onChange={this.props.handleEvent}/><br/>
                <label>Contact No</label>&nbsp;<input type="tel" maxLength="10" required onChange={this.props.handleEvent}/><br/>
                <button>Submit</button>
            </form>

            </div>
        )
    }
}

export default Presentation