import React from 'react';
import axios from 'axios'

class A extends React.Component{
    state = {}
    componentDidMount = () => {
        axios.get("https://jsonplaceholder.typicode.com/posts",{})
        .then((response) => {
            const data = response.data
            console.log(data)
            this.output = data.slice(0,5).map(p => {
                for(let i=0;i<p.length;i++){
                    if(i%3!==0)
                        return this.display(p)
                }
               
            })
            this.setState({
                abc : this.output
            })
            // console.log(output)
        }).catch(err => {
            console.log(err)
        })
    }

    display = (post) =>{
      
            return(
                <div>
                    <div className="post card" keys={post.id}>
                        <b>ID::&nbsp;&nbsp;</b>{post.id}
                        <div className="card-content">
                            <span className="card-title"><b>Title::&nbsp;</b>{post.title}</span>
                            <p>
                                <b>Post::</b><br/>{post.body}
                            </p>
    
                        </div>
    
                    </div>
                </div>
               
            )
        
    }
    output = ""
    render(){
        return(
            <div>{this.state.abc}</div>
        )
    }
}
export default A;