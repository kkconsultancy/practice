import React from 'react';
import axios from 'axios'
import Slider from 'react-slick'

class EX1 extends React.Component{

    state={}
    componentDidMount =()=>{

        axios.get("https://jsonplaceholder.typicode.com/posts",{})
        .then((response)=>{
            const data =response.data;
            console.log(data)
            this.op = data.slice(0,2).map(p=>{
                return this.op(p)
            })
            this.setState({
                ex1: this.op
            })
        }).catch(err=>{
                console.log(err)
            })
       
    }
    op =(ob)=>{
        return(
            <div>
                <table>
                    <tbody>
                        <td colSpan="4">{this.display(ob)}</td>
                    </tbody>
                </table>
            </div>
        )
    }
    display=(obj)=>{
        return(
            <div>    
                <div>
                    <b>UserID:</b>{obj.id}
                </div><br/>
                <div>
                    <b>Title:</b>{obj.title}
                </div><br/>
            </div>
                    
        )
    }
    render(){
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };
        return(
            <div>
                <Slider {...settings}>
                <div>{this.state.ex1}</div>
                </Slider>
                
            </div>
        )
    }
}
export default EX1;