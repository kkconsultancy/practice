import React,{Component} from 'react';

class Register extends Component{
    
    state={
       // error:"err"
    }
    handleEvent = (e)=>{
        const name=e.target.name;
        const value=e.target.value;
        this.setState(
            {
                [name]:value
            }
        )
        console.log(this.state)
        
    }
    validate_regno =() =>{
        let value = document.form1.Hno.value;
        console.log(value);
        var pattern= /^[0-9]{2}[A-Z|0-9]{8}$/
        if(value.length<10 || value.length>10)
            console.log("invalid- no 10 chars");
        else{
            if(!value.match(pattern)){
                //alert("Invalid Reg no!!");
                console.log("invalid regno.")
                document.form1.Hno.focus();

            }
            else
                console.log("valid regno.");
        }
    }

    validate_fname =() =>{
        let value = document.form1.fname.value;
        //console.log(value);
        var pattern= /^[A-Z|a-z| ]+$/
        if(!value.match(pattern)){
            console.log("invalid name- only alphabets required")
        }
        else    
            console.log("valid--"+value);
    }

    validate_lname =() =>{
        let value = document.form1.lname.value;
        //console.log(value);
        var pattern= /^[A-Z|a-z| ]+$/
        if(!value.match(pattern)){
            console.log("invalid name- only alphabets required")
        }
        else    
            console.log("valid--"+value);
    }

    validate_dept =()=>{
        let value= document.form1.dept.value;
        //console.log(value);
        if(value==="Default"){
           console.log("Select any dept")
            document.form1.dept.focus();
        }
        else
            console.log("valid--"+value);
    }
    validate_year =()=>{
        let value= document.form1.yr.value;
        console.log(value);
        if(value==="Default"){
            console.log("Select any year")
            document.form1.dept.focus();
        }
        else
            console.log("valid--"+value);
    }
    validate_clg =()=>{
        let value= document.form1.clg.value;
        console.log(value);
        if(value==="Default"){
            console.log("Select any college")
            document.form1.dept.focus();
        }
        else
            console.log("valid--"+value);
    }

    validate_mobile = () => {
        let value = document.form1.phno.value;
        console.log(value);
        var pattern = /^\d{10}$/;
        if(value.length===""||value.length<10||value.length>10)
            console.log("Invalid mobile no.")
        else{
            if(value.length===10)
                if(!value.match(pattern)){
                   console.log("only numeric values allowed")
                }
                else{
                    console.log("valid--"+value);
                }
        }
        
    }
    validate_email =()=>{
        let value = document.form1.mail.value;
        //console.log(value);
        //var pattern1= /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var pattern= /^[A-z0-9]{1,}@[A-z0-9]{1,}\..+$/;
        //var pattern=/^\w+$/;
        if(!value.match(pattern)){
            console.log("Invalid email");
        }
    } 
    validate_gen=()=>{
        let value=document.form1.gen.value;
        let x=0;
        if(value.checked){
            x=x+1;
        }
        if(x==0)
            console.log("choose gen")
        else    
            console.log("valid --"+value)
    }  
    submit =(e) =>{
       e.preventDefault();
        console.log("Submitted")


    }
    
    render(){
        return(
            <div>
            <h1>Registration form</h1>
            <form name="form1">
                <label>Hallticket Number&nbsp;</label>
                <input type="text" name="Hno" required onInput={this.handleEvent}  onChange={this.validate_regno} /><br/><br/>
                
                <label>FirstName</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="text" name="fname" minLength="5" maxLength="20" required onInput={this.handleEvent} onChange={this.validate_fname}/><br/><br/>

                LastName&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="text" name="lname" minLength="5" maxLength="20" required onInput={this.handleEvent} onChange={this.validate_lname}/><br/><br/>

                Gender&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="gen" required onChange={this.handleEvent}/>Male&nbsp;
                    <input type="radio" name="gen"  required onChange={this.handleEvent}/>Female<br/><br/>

                Course&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="course" required onChange={this.handleEvent}/>B.Tech&nbsp;
                    <input type="radio" name="course" required onChange={this.handleEvent}/>Diploma<br/><br/>

                <label>Department</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <select selected="" name="dept"required onInput={this.handleEvent} onChange={this.validate_dept}>
                    <option value="Default">---</option>
                    <option value="cse">CSE</option>
                    <option value="ece">ECE</option>
                    <option value="eee">EEE</option>
                    <option value="me">ME</option>
                    <option value="ce">CE</option>
                </select><br/><br/>

                <label>Year</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <select  name="yr"required onInput={this.handleEvent} onChange={this.validate_year}>
                    <option value="Default">---</option>
                    <option value="1">I</option>
                    <option value="2">II</option>
                    <option value="3">III</option>
                    <option value="4">IV</option>
                </select><br/><br/> 

                <label>College</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <select name="clg"required onInput={this.handleEvent} onChange={this.validate_clg}>
                    <option value="Default">---</option>
                    <option value="site">SITE</option>
                    <option value="vasavi">VASAVI</option>
                    <option value="wise">WISE</option>
                    <option value="visit">VISIT</option>
                </select><br/><br/>

                <label>E-Mail</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="email" name="mail" required onInput={this.handleEvent} onChange={this.validate_email}/><br/><br/>

                <label>Contact No</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="tel" name="phno" maxLength="10" required onInput={this.handleEvent} onChange={this.validate_mobile}/><br/><br/>

                <button onClick={this.submit}>Submit</button>
                {this.state.name}   
            </form>
            </div>

            
        )
    }
}
export default Register;