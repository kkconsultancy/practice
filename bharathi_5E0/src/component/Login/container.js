import React,{Component} from 'react'
import axios from 'axios'
import Presentation from './presentation'

class Container extends Component{

    state={
        key : "value"
    }
    componentDidMount =()=>{

        axios.get("https://jsonplaceholder.typicode.com/photos",{})
        .then(response=>{
            const set =response.data
            this.op= set.map(photos=>{
                return this.display(photos)
            })
            this.setState({
                set :set,
                abc: this.op
            })
        }).catch(err=>{
            console.log(err)
        })
    }
    submit=(e)=>{
        const name =e.target.name
        const value =e.target.value
        this.setState({
           [name]: value
        })
        this.filters(e)
    }

    filters =(e)=>{
        var name= e.target.name;
        if(name==="title"){

            let value= e.target.value;
            //let text="title  with aeiou"
            console.log(value);
            const set2=this.state.set.filter(res=>res.title.match(/^[aeiou]/)).map(records=>{
                return (this.display(records))
            })
            this.setState({
                set2: set2
            })
        }
        else if(name==="Even"){
           // let text="even id"
            //console.log(text)
            const set2=this.state.set.filter(res=>res.id%2===0).map(records=>{
                return (this.display(records))

            })
            this.setState({
                set2: set2
            })
        
        }
        else if(name==="search"){
            let value=e.target.value;
            //let text="Searching "
            //console.log(text)
            const set2=this.state.set.filter(res=>res.title.match(value)).map(records=>{
                return (this.display(records))

            })
            this.setState({
                set2: set2
            })
        }
    }

    display =(p)=>{
        
        return(
            <div>
                <b>AlbumID:</b>{p.id}<br/>
                <b>Title:</b>{p.title}<br/>
            </div>
        )
    }
    render(){
        return(

            <Presentation
                {...this.props}
                {...this.state}
                submit={this.submit}
            />


        )
    }
}

export default Container;
