import React,{Component} from 'react';

import {Link} from 'react-router-dom';

class Login extends Component{
    state = {
        email : '',
        password : ''
    }
    detectedChange = (e) => {
        this.setState({
            [e.target.id] : e.target.value
        })
    }
    detectedSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
    }
    render(){
        return(
            <div className="container center">
                <h1>LOGIN</h1>
                <form className="container" onSubmit={this.detectedSubmit}>
                    <span className="left">Email / Phone no.</span>
                    <input id="email" onChange={this.detectedChange}></input>
                    <span className="left">Password</span>
                    <input id="password" type="password" onChange={this.detectedChange}></input>
                    <button className="btn">LOGIN</button>
                    <div>
                    <Link to="/forgot-password"> Forgot Password </Link> / 
                    <Link to='/register'> Register </Link>
                    </div>
                </form>
            </div>
        )
    }
}

export default Login