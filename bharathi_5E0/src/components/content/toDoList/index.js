import React,{Component} from 'react';

import AddItem from './addToDo';

class ToDoList extends Component{
    render(){
        const {jobs} = this.props
        const TasksList = (jobs.length)?(
            jobs.map(job =>{
                return (
                    <div key={job.id} className="collection-item" onClick={() => {this.props.removeJob(job.id)}}>
                        <span>{job.message}</span>
                    </div>
                )
            })
        ):(
            <p>No Dues today boy:)</p>
        )
        return(
            <div className="container toDo">
                <h1>TO DO LIST</h1>
                <div className="collection">
                    {TasksList}
                </div>
                <AddItem createJob={this.props.createJob}/>
            </div>
        )
    }
}

export default ToDoList