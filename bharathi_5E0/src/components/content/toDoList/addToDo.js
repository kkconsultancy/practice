import React,{Component} from 'react';

class AddToDo extends Component{
    state = {
        message : ''
    }
    detectedChange = (e) => {
        const message = e.target.value
        this.setState({
            message : message
        })
    }
    detectedSubmit = (e) => {
        e.preventDefault()
        this.props.createJob(this.state.message)
        this.setState({
            message : ''
        })
    }
    render(){
        return (
            <form onSubmit={this.detectedSubmit}>
                <input onChange={this.detectedChange} value={this.state.message}></input>
                <button className="btn waves-effect waves-light">ADD</button>
            </form>
        )
    }
}

export default AddToDo