import React,{Component} from 'react';

class Footer extends Component{
    render(){
        return(
            <nav className="nav-wrapper blue">
                <ul className="center">
                    <li className="social">
                        <a href="https://developerswork.online" target="_blank">Copyrights @ Developers@Work</a>
                    </li>
                </ul>
            </nav>
        )
    }
}

export default Footer