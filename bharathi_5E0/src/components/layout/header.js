import React,{Component} from 'react';

import { NavLink } from 'react-router-dom';

class Header extends Component{
    render(){
        return(
            <nav className="nav-wrapper red darken-3">
                <div className="">
                    <a className="brand-logo">
                        <img src="https://i0.wp.com/developerswork.online/wp-content/uploads/2019/03/DESIGNS-Copy.png?w=360&ssl=1"></img>
                    </a>
                </div>
                <ul className="right">
                    <li>
                        <NavLink to="/">HOME</NavLink>
                    </li>
                    <li>
                        <NavLink to="/app">APP</NavLink>
                    </li>
                    <li>
                        <NavLink to="/login">LOGIN</NavLink>
                    </li>
                </ul>
            </nav>
        )
    }
}

export default Header