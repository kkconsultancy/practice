import React from 'react';

class csvtojson extends  React.Component
{
    state={
        data : {}
    }
    componentDidMount=()=>{
        const csvfile= "https://firebasestorage.googleapis.com/v0/b/site-org-automation.appspot.com/o/test.csv?alt=media&token=4fe45dee-3b91-4b70-bfb6-b6638fb9a3b3'"
        const csv = (require('csvtojson'))()
        fetch(csvfile,{

        })
        .then(response=>response.text())
        .then(response=>{ 
            console.log(response)
            csv.fromString(response)
            .then(response=>{
             console.log(response)
             this.op= response.slice(0,20).map(response=>{
                this.state.data[response.PassengerId] = response
                return this.print(this.state.data[response.PassengerId])
            })
            this.setState({
                abc: this.op
            })             
        }).catch(err=>{
            console.log(err)
        })
        
        
   }).catch(err=>{
       console.log(err)
   })  
}


print=(p)=>{
        
    console.log(p.PassengerId)
    
    return(
        <tr key={p.PassengerId}>
            <td>{p.PassengerId}</td> 
            <td>{p.Pclass} </td> 
            <td>{p.Name}</td>
            <td>{p.Sex}</td>
        </tr>
    )
}    
render(){
    return(
        <div>
           <table>
               <tbody>
                   <tr>
                       <th>Passenger ID</th>
                       <th>Passenger Class</th>
                       <th>Passenger Name</th>
                       <th>Passenger sex</th>
                   </tr>
                   {this.state.abc}
                </tbody>
           </table>
        </div>
    )
}
    
}

export default csvtojson;