import React,{Component} from 'react';

class regform extends Component{
    state={}
    handle =(e)=>{
        var name=e.target.name;
        var value=e.target.value;
        this.setState({
            [name]: value
        })
        console.log(value)
        this.validate();
    }
    state={}
    handle1 =(e)=>{
        var name=e.target.name;
        var value=e.target.value;
        this.setState({
            [name]: value
        })
        console.log(value)
        this.validatenum();

    }
    state={}
    handleemail=(e)=>{
        var name=e.target.name;
        var value=e.target.value;
        this.setState({
            [name]: value
        })
        console.log(value)
        this.validateemail();

    }
    handleregd=(e)=>{
        var name=e.target.name;
        var value=e.target.value;
        this.setState({
            [name]: value
        })
        console.log(value)
        this.validateregd();

    }
    handlebranch=(e)=>{
        var name=e.target.name;
        var value=e.target.value;
        this.setState({
            [name]: value
        })
        console.log(value)
    }

    validate =()=>{
        var value = document.Regform.Firstname.value;
        var pattern=/^[A-Z|a-z| ]+$/;
        if(value.match(pattern))
        {
                console.log("valid");
        }
        else{
            console.log("invalid");
        }
    }
    validatenum =()=>{
        var value = document.Regform.Number.value;
        var pattern=/^\d{10}$/;
        if(value.match(pattern))
        {
                console.log("valid");
        }
        else{
            console.log("invalid");
        }
    }
    validateemail =()=>{
        var value = document.Regform.Gmail.value;
        var pattern=/^([a-zA-Z0-9]+)@([a-zA-Z]+)\.([a-zA-Z]+)$/;
        if(value.match(pattern))
        {
                console.log("valid");
        }
        else{
            console.log("invalid");
        }
    }
    validateregd =()=>{
        var value = document.Regform.RegdNo.value;
        var pattern=/^[0-9]{2}[A-Za-z]{1}[0-9]{1}[0-9]{1}[A-Za-z]{1}[0-9]{2}[A-Za-z0-9]{2}$/;;
        if(value.match(pattern))
        {
                console.log("valid");
        }
        else{
            console.log("invalid");
        }
    }
    onRadioChange=(e)=>{
        var name=e.target.name;
        var value=e.target.value;        
        this.setState({[name]: value})
        console.log(value);
    }
    render(){
        return(
            <div>
                <h3>Registration Form</h3>
                <form name="Regform">
                    Username:<input type="text" name="Firstname" onChange={this.handle}/><br/>
                    contactNo:<input type="number" name="Number" onChange={this.handle1} /><br/>
                    email<input type="email" name="Gmail" onChange={this.handleemail}/><br/>
                    GENDER<br/><input type="radio"
                            name="radio1" 
                            value="male"
                            checked={this.state.radio1 === "male"} 
                            onChange={this.onRadioChange}/>male<br/>
                           <input type="radio" 
                           name="radio1"
                           value="fem" 
                           checked={this.state.radio1 === "fem"} 
                           onChange={this.onRadioChange}/>fem<br/>
                    Regd No:<input type="text" name="RegdNo" onChange={this.handleregd}/><br/>
                    Branch:<select name="Branch" onChange={this.handlebranch}>
                        <option value="CSE">CSE</option>
                        <option value="ECE">ECE</option>
                        <option value="IT">IT</option>
                        <option value="EEE">EEE</option>
                    </select><br/>
                    <button onClick={this.submit}>Submit</button>
                 
                </form>

                
            </div>
           
        )
    }
}
export default regform;