import React from 'react';

class json2csv extends  React.Component
{
    state={
        data : {}
    }
    componentDidMount=()=>{
        const { Parser } = require('json2csv');
        const fields = ['Empid', 'Empname', 'Empdept','Empsalary'];
        const Empdata = [
                {
                    "Empid": 1,
                    "Empname": "abc",
                    "Empdept": "testing",
                    "Empsalary": 40000
                }, {
                    "Empid": 2,
                    "Empname": "xyz",
                    "Empdept": "production",
                    "Empsalary": 30000
                }, {
                    "Empid": 3,
                    "Empname": "dbc",
                    "Empdept": "research",
                    "Empsalary": 60000
                }, {
                    "Empid": 4,
                    "Empname": "abc",
                    "Empdept": "marketing",
                    "Empsalary": 35000
                }, {
                    "Empid": 5,
                    "Empname": "ijk",
                    "Empdept": "testing",
                    "Empsalary": 45000
                }
        ];
 
        const json2csvParser = new Parser({ fields });
        const csv = json2csvParser.parse(Empdata);
 
        console.log(csv);

        var csvContent =  "data:text/csv;charset=utf-8,"+csv;
        var link = document.getElementById("file");
        link.setAttribute("href", encodeURI(csvContent));
        link.setAttribute("download", "emp_data.csv");
        link.textContent ="Download csv file "; 
        //link.click()
        console.log(link)
        
}
render(){
    return(
        <div>
            <a id="file">hii</a>
        </div>
    )
}
    
}

export default json2csv;