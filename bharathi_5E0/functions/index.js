const functions = require('firebase-functions');

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.firebaseAuth = functions.https.onRequest((request, response) => {
    const config = {
        hostname : request.hostname,
        method : request.method,
        parameters : request.body
    };

    console.log(config);
    
    if(config.method !== 'POST'){
        response.send('NICE TRY :)');
        return;
    }
    const server = 'us-central1-react-auth-servs.cloudfunctions.net';
    // if(config.hostname !== 'developerswork.online' || config.hostname !== 'localhost' || config.hostname !== server){
    //     response.send(':) YOU GOT BE KIDDING!!!');
    //     return;
    // }
    if(config.parameters.auth !== 'I AM IRON MAN'){
        response.send('DUDE I GOT PROTECTION FROM MOST POWERFUL AI :)');
        return;
    }

    const auth = {
        apiKey: "AIzaSyDVac7m6mov6-VJi4fDLfR7JW_F5rhATiQ",
        authDomain: "react-auth-servs.firebaseapp.com",
        databaseURL: "https://react-auth-servs.firebaseio.com",
        projectId: "react-auth-servs",
        storageBucket: "react-auth-servs.appspot.com",
        messagingSenderId: "15624128449",
        appId: "1:15624128449:web:c0213140e2dba026"
    };
    response.send(auth);
});
