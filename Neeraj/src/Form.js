import React,{Component} from 'react';

class Form1 extends Component{
    state = {
        Hallticketno : '',
        Firstname : '',
        lastname :'',
        email:'',
        male: '',
        resetDisabled: false,
        Number: '',
        isGoing: '',
        Department:'',
        error:'',
        gender:''
        }
        handelchange = (e)=>{
            this.validate(e.target)
            this.setState({
                
                [e.target.name]: e.target.value,
                }
                );
                {console.log(this.state)}
            
            
        };
        validate = (e)=>{
            let error=this.state.error
            
            if(e.name==="Hallticketno"){
                console.log(e.value)
                if(!e.value.match(/[A-Za-z]/)) {
                    error="please enter the hall ticket name";
                    this.state.error=error;
                    console.log(this.state) 
                    return false;
                  }  
                
            }
            if(e.name==="Firstname"){
                console.log(e.value)
                if(!e.value.match(/[A-Za-z]/)) {
                    error="please enter the valid name";
                    this.state.error=error;
                    console.log(this.state)
                    return false;
                  }else{
                    this.state.error="";
                  }
            }
            if(e.name==="lastname"){
                console.log(e.value)
                if(!e.value.match(/[A-Za-z]/)) {
                    error="please enter the valid name";
                    this.state.error=error;
                    console.log(this.state)
                    return false;
                  }else{
                    this.state.error="";
                  }
            }
            if(e.name==="email"){
                console.log(e.value)
                if(!e.value.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)) {
                    error="please enter the valid email";
                    this.state.error=error;
                    console.log(this.state)
                    return false;
                  }else{
                    this.state.error="";
                  }
            }
            if(e.name==="Number"){
                console.log(e.value)
                if(!e.value.match(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)) {
                    error="please enter the valid number";
                    this.state.error=error;
                    console.log(this.state)
                    return false;
                  }else{
                    this.state.error="";
                  }
            }

        }
        Submit = (e)=>{
            e.preventDefault()
            this.setState({
                
                [e.target.name]: e.target.value,
                
                }
                
                )
                
        }
        
       
    render(){
        
        return(
        <form onSubmit={this.Submit}>
            <label>Hall ticket no</label>
            <input type="text" name="Hallticketno" value={this.state.Hallticketno} onChange={this.handelchange}/><br/>
            <label >Firstname</label>
            <input type="text" name="Firstname" value={this.state.Firstname} onChange={this.handelchange}/><br/>
            <label >lastname</label>
            <input type="text"  name="lastname" value={this.state.lastname} onChange={this.handelchange}/><br/>
            <label>Email</label>
            <input type="text"  name="email" value={this.state.email} onChange={this.handelchange}/><br/>
            <label>Gender</label><br/>
            <input type="radio" name="gender" value="male" checked={this.state.gender === "male"} onChange={this.handelchange}/> <label>Male</label><br/>
            <input type="radio" name="gender" value="female" checked={this.state.gender === "female"} onChange={this.handelchange}/> <label>Female</label><br/>
            <label >Number</label>
            <input type="text"  name="Number" value={this.state.Number} onChange={this.handelchange}/><br/>
            <select name="Department" value={this.state.Department} onChange={this.handelchange}>
        <option value="cse">cse</option>
        <option value="it">it</option>
        <option value="ece">ece</option>
      </select>
      <button >cliclk</button>
      <div>{this.state.error}</div>
            
        </form>
        );
    }

}
export default Form1