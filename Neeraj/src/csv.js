import React from 'react'

class Csv_Json extends React.Component{

    state={
        data : {}
    }
    componentDidMount=()=>{
        const csv2json = (require('csvtojson'))()
        const csvFile = 'https://firebasestorage.googleapis.com/v0/b/site-org-automation.appspot.com/o/test.csv?alt=media&token=4fe45dee-3b91-4b70-bfb6-b6638fb9a3b3'
        fetch(csvFile,{

        })
        .then(res=>res.text())
        .then(res=>{ 
            console.log(res) //CSV
        
            csv2json.fromString(res)
            .then(res=>{
                console.log(res)         //JSON
                 this.op= res.map(res=>{
                    
                    return this.display(res)
                   
                })
                this.setState({
                    op: this.op
                })
                              
            }).catch(err=>{
                console.log(err)
            })
            
            
       }).catch(err=>{
           console.log(err)
       })  
    }
   

    display=(r)=>{
        
        console.log(r.PassengerId)
        
        return(
            <tr>
                <td>
                    
                     {r.PassengerId} 
                </td> 
                <td>{r.Pclass}</td>
                <td>{r.Name}</td>
                <td>{r.Sex}</td>
                <td>{r.Age}</td>
            </tr>
        )
    }    
    render(){
        return(
            <div>
               <table>
                   <tbody>
                        {this.state.op}
                       
                   </tbody>
               </table>
                
              
            </div>
        )
    }
}

export default Csv_Json;