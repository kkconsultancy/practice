import React from 'react'
import Presentation from './presentation'
import axios from 'axios'

class Container extends React.Component{
    state = {
        key : "value",
        he : " "
    }
    componentDidMount(){
        axios.get(`https://jsonplaceholder.typicode.com/todos`)
        .then(res => {
        const jsx = res.data
                this.setState({
                    jsx : jsx
                }) 
                console.log(jsx)
        }).catch(err => console.log(err))
    }
    submit = (e) => {
        
         this.setState({
            [e.target.name] : e.target.value
        })

        this.partion(e)

    }

    createJSX = (data,count=0) => {
        if(count===0){
        return( 
          
       <div> {data.id}<div> 
           </div>
           <div>
           {data.title.toUpperCase()}</div>
           {data.userId}
        </div>)}
       else{
        return( 
            
        <div> {data.id}<div> 
            </div>
            <div>
            {data.title.toUpperCase()}</div>
            {data.userId}
            <lable>count</lable>{count}
         </div>)
       }
   
       }

    partion = (e)=>{
        console.log(this.state.he)
        if(e.target.name ==="search"){
            let he = e.target.value
            console.log(he)
           const jsx2= this.state.jsx.filter(record => record.title.match(he)).map((data)=>{
            
              
               return this.createJSX(data)
               

            })
            this.setState({
                jsx2 : jsx2
            })
        
        }else{
        if(e.target.value==="title with aeiou"){
            
           const jsx2= this.state.jsx.filter(record => record.title.match(/^[aeiou]{1}/)).map((data)=>{
               return this.createJSX(data)
               

            })
            console.log(jsx2)
            this.setState({
                jsx2 : jsx2
            })
        }
        else if(e.target.value==="evens"){
            
            const jsx2= this.state.jsx.filter(record => record.userId===1).map((data)=>{
                return this.createJSX(data)
            })
             console.log(jsx2)
             this.setState({
                 jsx2 : jsx2
             })
         }
         else if(e.target.value==="count"){
            let count=0
            const jsx2= this.state.jsx.filter(record => record.userId===1).map((data)=>{
                count +=1
                return this.createJSX(data,count)
            })
             console.log(jsx2)
             this.setState({
                 jsx2 : jsx2
             })
         }
         else  if(e.target.value==="skip1"){
            let count=0
            const jsx2= this.state.jsx.map((data)=>{
                count +=1
                if(count%2 === 0)
                return this.createJSX(data,count)
            })
             console.log(jsx2)
             this.setState({
                 jsx2 : jsx2
             })
         }else {
                this.setState({
                    jsx2 : ""
                })

             }}

            
        
    }
    
    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                submit={this.submit}
            />
        )
    }
}

export default Container